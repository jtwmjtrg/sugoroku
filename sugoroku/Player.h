
class Player {
private:
	int Ppoint; //キャラがいる場所
	int Rpoint; //キャラの初期位置(リスポーン地点)
	bool QueenFlag; //姫所有判定

	int Gr_char; //キャラのグラフィック


public:
	Player(int No); //コンストラクタ、引数にプレイヤー番号を取る
	~Player();

	void death(); //死亡時判定
	void update(); //動き
	void draw(); //描画

	int Ppointgetter(); //プレイヤーの位置のゲッタ
	void Ppointsetter(); //プレイヤーの位置のセッタ
	bool Queengetter(); //姫の所持判定のゲッタ
	void Queensetter(); //姫の所持判定のセッタ
};