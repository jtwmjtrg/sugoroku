typedef struct MapData
{
	int MapNum;  //マップ番号
	int MapEffect; //マップ効果
	int x; // マップX座標
	int y; //マップY座標
	
};



class Map{
private:
	MapData MData[41];	// マップデータ
	int Gr_chip[9];		// 画像

public:
	Map();
	~Map();
	MapData GetMapData(int id);

};